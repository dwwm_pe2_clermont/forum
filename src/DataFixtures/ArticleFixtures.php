<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {

        $article = new Article();
        $article->setImage("https://images.caradisiac.com/images/4/3/8/4/204384/S0-avec-son-e-3008-obese-peugeot-montre-les-limites-du-tout-electrique-770192.jpg");
        $article->setTitre("Le nouveau E3008 est là !");
        $article->setContenu("OUAOUH SUPER");
        $article->setAuteur($this->getReference("adelorme"));
        $article->setCategory($this->getReference("tech"));
        // $product = new Product();
         $manager->persist($article);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            AuteurFixtures::class
        ];
    }
}
