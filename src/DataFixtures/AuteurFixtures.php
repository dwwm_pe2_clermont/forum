<?php

namespace App\DataFixtures;

use App\Entity\Auteur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AuteurFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
       $auteur = new Auteur();
       $auteur->setNom("Delorme");
       $auteur->setPrenom("Aurélien");
       $auteur->setEmail("adelorme-ext@formateur-humanbooster.com");
       $this->addReference("adelorme", $auteur);
       $manager->persist($auteur);

       $faker = Factory::create("fr_FR");
       for($i = 0; $i<499; $i++){
           $auteur = new Auteur();
           $auteur->setEmail($faker->email);
           $auteur->setNom($faker->lastName);
           $auteur->setPrenom($faker->firstName);
           $manager->persist($auteur);
       }
        $manager->flush();
    }
}
