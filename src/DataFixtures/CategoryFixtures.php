<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $categoryTech = new Category();
        $categoryTech->setLabel("Technology");
        $this->addReference("tech", $categoryTech);
        $manager->persist($categoryTech);

        $categorySport = new Category();
        $categorySport->setLabel("Sport");
        $manager->persist($categorySport);

        $manager->flush();
    }
}
